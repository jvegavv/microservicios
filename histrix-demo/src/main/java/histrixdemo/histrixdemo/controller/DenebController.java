package histrixdemo.histrixdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import histrixdemo.histrixdemo.hystrix.SadrClient;

@RestController
@RequestMapping("deneb/")
public class DenebController {

    

    @Autowired
    private SadrClient sadrClient;

    @RequestMapping(method=RequestMethod.POST, value="pingSadr")    
    public void pingSadr() {
    
         sadrClient.pingSadr();
    }
    
    
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}