package histrixdemo.histrixdemo.hystrix;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;




@Component
public class SadrClient {
        
	@Autowired	
	protected RestTemplate restTemplate;


    @HystrixCommand(fallbackMethod="retrieveFallbackPingSadr")
    public void pingSadr(){
    	
    	URI targetUrl= UriComponentsBuilder.fromUriString("SADR")  // Build the base link
    		    .path("/consumir")                            // Add path
    		    .queryParam("palabra", "JAVA")                                // Add one or more query params
    		    .build()                                                 // Build the URL
    		    .encode()                                                // Encode any URI items that need to be encoded
    		    .toUri();                                                // Convert to URI


    		
        
        String palabra = restTemplate.getForObject(targetUrl , String.class);
        
        System.out.println("resultado --> "+palabra );
    }
    
    

    public void retrieveFallbackPingSadr(){
        System.out.println("Error pinging sadr. This is a fallback message");
    }
  
}