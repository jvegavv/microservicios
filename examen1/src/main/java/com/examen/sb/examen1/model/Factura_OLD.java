package com.examen.sb.examen1.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@Table(name="factura")
public class Factura_OLD implements Serializable {

	
	@Id	
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_factura")	
	private Integer idFactura;
	
	@Column	
	@Temporal(TemporalType.DATE)
	private Date fecha;

	@JoinColumn(name="id_cliente_fk")
	@ManyToOne(fetch=FetchType.LAZY)
	private Cliente_OLD idClienteFk;
	
	
	@OneToMany(mappedBy="factura" , fetch=FetchType.LAZY, cascade=CascadeType.REMOVE)	
	private List<DetalleFactura> detalleFactura;
		
				
	public List<DetalleFactura> getDetalleFactura() {
		return detalleFactura;
	}
	public void setDetalleFactura(List<DetalleFactura> detalleFactura) {
		this.detalleFactura = detalleFactura;
	}
	public Integer getIdFactura() {
		return idFactura;
	}
	public void setIdFactura(Integer idFactura) {
		this.idFactura = idFactura;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Cliente_OLD getIdClienteFk() {
		return idClienteFk;
	}
	public void setIdClienteFk(Cliente_OLD idClienteFk) {
		this.idClienteFk = idClienteFk;
	}

	
}
