package com.examen.sb.examen1.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="detalle_factura")
public class DetalleFactura implements Serializable {

	@Id	
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_detalle_factura")
	private Integer idDetalleFactura;
	
	@JoinColumn(name="id_factura_fk")	
	@ManyToOne(fetch=FetchType.LAZY)
	private Factura_OLD factura;
	
	@JoinColumn(name="id_producto_fk")	
	@ManyToOne(fetch=FetchType.LAZY)
	private Producto producto;
		
	@Column
	private Integer precio;
	
	@Column
	private Integer cantidad;
	
		
	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Integer getIdDetalleFactura() {
		return idDetalleFactura;
	}

	public void setIdDetalleFactura(Integer idDetalleFactura) {
		this.idDetalleFactura = idDetalleFactura;
	}

	public Factura_OLD getFactura() {
		return factura;
	}

	public void setFactura(Factura_OLD factura) {
		this.factura = factura;
	}

	public Integer getPrecio() {
		return precio;
	}

	public void setPrecio(Integer precio) {
		this.precio = precio;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
		
	
}
