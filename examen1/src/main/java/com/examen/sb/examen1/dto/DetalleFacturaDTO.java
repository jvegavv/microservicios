package com.examen.sb.examen1.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class DetalleFacturaDTO implements Serializable {
	
	
	private Integer idDetalleFactura;
	private FacturaDTO factura;
	private ProductoDTO producto;
	private Integer precio;
	private Integer cantidad;
	
	
	public Integer getIdDetalleFactura() {
		return idDetalleFactura;
	}
	public void setIdDetalleFactura(Integer idDetalleFactura) {
		this.idDetalleFactura = idDetalleFactura;
	}
	public FacturaDTO getFactura() {
		return factura;
	}
	public void setFactura(FacturaDTO factura) {
		this.factura = factura;
	}
	public ProductoDTO getProducto() {
		return producto;
	}
	public void setProducto(ProductoDTO producto) {
		this.producto = producto;
	}
	public Integer getPrecio() {
		return precio;
	}
	public void setPrecio(Integer precio) {
		this.precio = precio;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
		

}
