package com.examen.sb.examen1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.examen.sb.examen1.dto.ClienteDTO;
import com.examen.sb.examen1.dto.FacturaDTO;
import com.examen.sb.examen1.service.PruebaService;




@RestController
@RequestMapping("/")
public class PruebaController {
	
	
	@Autowired
	PruebaService pruebaservice;
	
	@RequestMapping(value="/clientes/{rut}", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ClienteDTO> buscarCliente(
			@PathVariable(value = "rut") Integer rut
			) {
		
		ClienteDTO retorno= pruebaservice.buscarCliente(rut);
		
		if (retorno == null){
			
			return new ResponseEntity<ClienteDTO>(retorno, HttpStatus.NOT_FOUND);
			
		}else{
		
			return new ResponseEntity<ClienteDTO>(retorno, HttpStatus.OK);
		}
		
		
	}
	
	
	@RequestMapping(value="/clientesEntidad/{rut}", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ClienteDTO> buscarClienteEntidad(
			@PathVariable(value = "rut") Integer rut
			) {
		
		ClienteDTO retorno= pruebaservice.buscarClienteEntidad(rut);
		
		if (retorno == null){
			
			return new ResponseEntity<ClienteDTO>(retorno, HttpStatus.NOT_FOUND);
			
		}else{
		
			return new ResponseEntity<ClienteDTO>(retorno, HttpStatus.OK);
		}
		
		
	}
	
	@RequestMapping(value="/facturasEntidad/{idFactura}", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FacturaDTO> buscarFacturaEntidad(
			@PathVariable(value = "idFactura") Integer idFactura
			) {
		
		FacturaDTO retorno= pruebaservice.buscarFacturaEntidad(idFactura);
		
		if (retorno == null){
			
			return new ResponseEntity<FacturaDTO>(retorno, HttpStatus.NOT_FOUND);
			
		}else{
		
			return new ResponseEntity<FacturaDTO>(retorno, HttpStatus.OK);
		}
		
		
	}
	
	
	@RequestMapping(value="/factura/crear", method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,  produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> crerFactura(
			@RequestBody FacturaDTO facturaDTO	
			) {
		
				
		Integer folio= pruebaservice.crerFactura(facturaDTO);
		
		return new ResponseEntity<Integer>(folio, HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/detalleFactura/borrar/{idDetalleFactura}", method=RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> borrarDetalleFactura(
			@PathVariable(value = "idDetalleFactura") Integer borrarDetalleFactura
			) {
		
				
		Integer folio= pruebaservice.borrarDetalleFactura(borrarDetalleFactura);
		
		return new ResponseEntity<Integer>(folio, HttpStatus.OK);
	}
	
	@RequestMapping(value="/factura/borrar/{idFactura}", method=RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> borrarFactura(
			@PathVariable(value = "idFactura") Integer idFactura
			) {
		
				
		Integer folio= pruebaservice.borrarFactura(idFactura);
		
		return new ResponseEntity<Integer>(folio, HttpStatus.OK);
	}

	

}
