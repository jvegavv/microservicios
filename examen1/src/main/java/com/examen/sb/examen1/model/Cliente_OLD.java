package com.examen.sb.examen1.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="cliente")
public class Cliente_OLD  implements Serializable {
	
	@Id	
	@Column(name="id_cliente")	
	private Integer idCliente; 
	
	@Column(name="razon_social")
	private String razonSocial;
	
	@Column(name="giro")	
	private String giro;
	
	@Column(name="dv")
	private String dv;
	
	
	@OneToMany(mappedBy="idClienteFk")
	private List<Factura_OLD> facturas;
	

	

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getGiro() {
		return giro;
	}

	public void setGiro(String giro) {
		this.giro = giro;
	}

	public String getDv() {
		return dv;
	}

	public void setDv(String dv) {
		this.dv = dv;
	}

	public List<Factura_OLD> getFacturas() {
		return facturas;
	}

	public void setFacturas(List<Factura_OLD> facturas) {
		this.facturas = facturas;
	}

	@Override
	public String toString() {
		return "Cliente_OLD [idCliente=" + idCliente + ", razonSocial=" + razonSocial + ", giro=" + giro + ", dv=" + dv
				+ ", facturas=" + facturas + ", getIdCliente()=" + getIdCliente() + ", getRazonSocial()="
				+ getRazonSocial() + ", getGiro()=" + getGiro() + ", getDv()=" + getDv() + ", getFacturas()="
				+ getFacturas() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	
	
	
	
	
	
	

}
