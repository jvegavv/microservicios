package com.examen.sb.examen1.service;

import com.examen.sb.examen1.dto.ClienteDTO;
import com.examen.sb.examen1.dto.FacturaDTO;
import com.examen.sb.examen1.model.Cliente_OLD;

public interface PruebaService {


	public ClienteDTO buscarCliente(Integer rut);
	
	public ClienteDTO buscarClienteEntidad(Integer rut);

	public FacturaDTO buscarFacturaEntidad(Integer idFactura);

	public Integer crerFactura(FacturaDTO facturaDTO);

	public Integer borrarDetalleFactura(Integer borrarDetalleFactura);

	public Integer borrarFactura(Integer idFactura);
	
	

}
