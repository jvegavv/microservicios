package com.examen.sb.examen1.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.examen.sb.examen1.dto.ClienteDTO;
import com.examen.sb.examen1.dto.DetalleFacturaDTO;
import com.examen.sb.examen1.dto.FacturaDTO;
import com.examen.sb.examen1.dto.ProductoDTO;
import com.examen.sb.examen1.model.Cliente_OLD;
import com.examen.sb.examen1.model.DetalleFactura;
import com.examen.sb.examen1.model.Factura_OLD;
import com.examen.sb.examen1.model.Producto;
import com.examen.sb.examen1.repository.ClienteRepositoty;
import com.examen.sb.examen1.repository.DetalleFacturaRepositoty;
import com.examen.sb.examen1.repository.FacturaRepositoty;
import com.examen.sb.examen1.service.PruebaService;

@Service
public class PruebaServiceImpl implements PruebaService {


	
	@Autowired
	private ClienteRepositoty clienteRepository;
	
	@Autowired
	private FacturaRepositoty facturaRepository;
	
	@Autowired
	private DetalleFacturaRepositoty detalleFacturaRepository;
	
	@Override
	public ClienteDTO buscarCliente(Integer rut) {
				
		
		//System.out.println("---------> "+rut);
		
		Cliente_OLD cliente = clienteRepository.findOne(rut);
		
		//System.out.println("---------> "+cliente.getRut());
		//System.out.println("---------> "+cliente.getRazonSocial());
		
		ClienteDTO clienteDTO = null;
	
		if (cliente != null){
			
			clienteDTO = new ClienteDTO();
			BeanUtils.copyProperties(cliente, clienteDTO);
		}
		
		return clienteDTO;
		
	}





	@Override
	public ClienteDTO buscarClienteEntidad(Integer rut) {
				
		Cliente_OLD cliente = clienteRepository.findOne(rut);
		ClienteDTO clienteDTO = null;
		List<FacturaDTO> listaFacturas = new ArrayList<FacturaDTO>();
		
	if (cliente != null){
			
			clienteDTO = new ClienteDTO();
			
			//BeanUtils.copyProperties( cliente , clienteDTO);
			
			clienteDTO.setIdCliente(cliente.getIdCliente());
			clienteDTO.setDv(cliente.getDv());
			clienteDTO.setGiro(cliente.getGiro());
			clienteDTO.setRazonSocial(cliente.getRazonSocial());
			
			/*
			FacturaDTO facturaDTO = null;		
								
			
			for ( Factura_OLD facturaEntidad : cliente.getFacturas()){
											
				facturaDTO = new  FacturaDTO();				
				BeanUtils.copyProperties( facturaEntidad , facturaDTO);
				listaFacturas.add(facturaDTO);
				
			}
			
			clienteDTO.setFacturas(listaFacturas);
				*/
			
		}
		
		//System.out.println("IMPRIMIR "+cliente.toString());
		return clienteDTO;
		
	}

	@Override
	public FacturaDTO buscarFacturaEntidad(Integer idFactura) {
	
		

		Factura_OLD factura = facturaRepository.findOne(idFactura);
		FacturaDTO facturaDTO = null;
		DetalleFacturaDTO detalleFacturaDTO = null;
		ProductoDTO productoDTO = null;
		ClienteDTO clienteDTO = new ClienteDTO();
		List<DetalleFacturaDTO> listaDetalleFacturaDTO = new ArrayList<DetalleFacturaDTO>();
		
		if (factura != null){
			facturaDTO = new FacturaDTO();
			//BeanUtils.copyProperties( factura , facturaDTO);	
			
			facturaDTO.setFecha(factura.getFecha());
			facturaDTO.setIdFactura(factura.getIdFactura());
			
			clienteDTO.setIdCliente(factura.getIdClienteFk().getIdCliente());
			clienteDTO.setDv(factura.getIdClienteFk().getDv());
			clienteDTO.setGiro(factura.getIdClienteFk().getGiro());
			clienteDTO.setRazonSocial(factura.getIdClienteFk().getRazonSocial());
			
			facturaDTO.setIdClienteFk(clienteDTO);
			
			for (DetalleFactura detalleFactura : factura.getDetalleFactura()){
				
				detalleFacturaDTO = new DetalleFacturaDTO();
				productoDTO = new ProductoDTO();
				
				detalleFacturaDTO.setIdDetalleFactura(detalleFactura.getIdDetalleFactura());
				detalleFacturaDTO.setCantidad(detalleFactura.getCantidad());
				detalleFacturaDTO.setPrecio(detalleFactura.getPrecio());
				
				productoDTO.setIdProducto(detalleFactura.getProducto().getIdProducto());
				productoDTO.setPrecio(detalleFactura.getProducto().getPrecio());
				productoDTO.setDescripcion(detalleFactura.getProducto().getDescripcion());
				
				
				detalleFacturaDTO.setProducto(productoDTO);
				
				listaDetalleFacturaDTO.add(detalleFacturaDTO);
				
			}
			
			facturaDTO.setDetalleFactura(listaDetalleFacturaDTO);
					
		}
	
		
		return facturaDTO;
		
	}


	@Override
	public Integer crerFactura(FacturaDTO facturaDTO) {
		
		
		Factura_OLD factura = new Factura_OLD();
		Cliente_OLD cliente = new Cliente_OLD();
		DetalleFactura  detalleFactura = null;
		Producto producto = null;		
		List<DetalleFactura> listaDetalleFactura = new ArrayList<DetalleFactura>();
		
							
		cliente.setIdCliente(facturaDTO.getIdClienteFk().getIdCliente());		
		factura.setIdClienteFk(cliente);
		facturaRepository.save(factura);
		
		
		
		//return factura.getIdFactura();
		
		for ( DetalleFacturaDTO detalleFacturaDTO  :  facturaDTO.getDetalleFactura()){
			
			detalleFactura = new DetalleFactura();
			producto = new Producto();			
			detalleFactura.setCantidad(detalleFacturaDTO.getCantidad());
			detalleFactura.setPrecio(detalleFacturaDTO.getPrecio());			
			producto.setIdProducto(detalleFacturaDTO.getProducto().getIdProducto());			
			detalleFactura.setProducto(producto);
			detalleFactura.setFactura(factura);		
			listaDetalleFactura.add(detalleFactura);
			
		}
		
		
		detalleFacturaRepository.save(listaDetalleFactura);
		
		return factura.getIdFactura();
		
	}





	@Override
	public Integer borrarDetalleFactura(Integer borrarDetalleFactura) {
	
		 detalleFacturaRepository.delete(borrarDetalleFactura);
		
		return 1;
		
	}





	@Override
	public Integer borrarFactura(Integer idFactura) {

		 facturaRepository.delete(idFactura);
		 
		 return 1;
		 
	}	
	
}
