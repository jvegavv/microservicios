package com.examen.sb.examen1.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.examen.sb.examen1.model.DetalleFactura;

public interface DetalleFacturaRepositoty extends JpaRepository<DetalleFactura, Integer> {


}
