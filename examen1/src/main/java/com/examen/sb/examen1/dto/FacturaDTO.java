package com.examen.sb.examen1.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class FacturaDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idFactura;		
	private Date fecha;	
	private ClienteDTO idClienteFk;
	private List<DetalleFacturaDTO> detalleFactura;
	
	public List<DetalleFacturaDTO> getDetalleFactura() {
		return detalleFactura;
	}

	public void setDetalleFactura(List<DetalleFacturaDTO> detalleFactura) {
		this.detalleFactura = detalleFactura;
	}

	public Integer getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(Integer idFactura) {
		this.idFactura = idFactura;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public ClienteDTO getIdClienteFk() {
		return idClienteFk;
	}

	public void setIdClienteFk(ClienteDTO idClienteFk) {
		this.idClienteFk = idClienteFk;
	}
	
	
	
	
}
