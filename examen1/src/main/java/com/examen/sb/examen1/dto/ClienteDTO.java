package com.examen.sb.examen1.dto;

import java.io.Serializable;
import java.util.List;

public class ClienteDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idCliente; 
	private String razonSocial;
	private String giro;
	private String dv;
	private List<FacturaDTO> facturas;
	
	
	public Integer getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getGiro() {
		return giro;
	}
	public void setGiro(String giro) {
		this.giro = giro;
	}
	public String getDv() {
		return dv;
	}
	public void setDv(String dv) {
		this.dv = dv;
	}
	public List<FacturaDTO> getFacturas() {
		return facturas;
	}
	public void setFacturas(List<FacturaDTO> facturas) {
		this.facturas = facturas;
	}
	
	

}
