package com.examen.sb.examen1.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="producto")
public class Producto implements Serializable {

	@Id	
	@Column(name="id_producto")
	private Integer idProducto;
	@Column
	private String descripcion;
	@Column
	private Integer precio;
		
	@OneToMany(mappedBy="producto" , fetch=FetchType.LAZY)		
	private List <DetalleFactura> detallesFactura;
		
	
	public List<DetalleFactura> getDetallesFactura() {
		return detallesFactura;
	}
	public void setDetallesFactura(List<DetalleFactura> detallesFactura) {
		this.detallesFactura = detallesFactura;
	}
	public Integer getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Integer getPrecio() {
		return precio;
	}
	public void setPrecio(Integer precio) {
		this.precio = precio;
	}
	
	
}
