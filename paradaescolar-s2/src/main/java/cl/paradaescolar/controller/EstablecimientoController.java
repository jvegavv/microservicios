package cl.paradaescolar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.paradaescolar.dto.EstablecimientoDTO;
import cl.paradaescolar.service.EstablecimientoService;

@RestController
@RequestMapping("/api/v1")
public class EstablecimientoController {
  @Autowired
  private EstablecimientoService establecimientoService;

  @GetMapping("/establecimientos/{id}")
  public EstablecimientoDTO findEstablecimiento(
    @PathVariable (value = "id") Integer id

  ) {
     return establecimientoService.findEstablecimiento(id);
  } 

  @GetMapping("/establecimientos")
  public void findEstablecimiento(
    
  ) {
      establecimientoService.findAll();
  } 


  @PostMapping(path="/establecimientos/crear", consumes = MediaType.APPLICATION_JSON_VALUE )
  public ResponseEntity<EstablecimientoDTO>  createEstablecimiento(
    @RequestBody EstablecimientoDTO establecimientoDTO	

  ) {

    EstablecimientoDTO respuesta=  establecimientoService.createEstablecimiento(establecimientoDTO);

    return new ResponseEntity<EstablecimientoDTO>(respuesta, HttpStatus.CREATED);
  } 

  @DeleteMapping(path="/establecimientos/{id}" )
  public ResponseEntity<Boolean>  createEstablecimiento(
    @PathVariable(value="id") Integer id

  ) {

    Boolean respuesta=  establecimientoService.borrarEstablecimiento(id);

    return new ResponseEntity<Boolean>(respuesta, HttpStatus.OK);
  }


}