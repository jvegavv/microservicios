package cl.paradaescolar.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.paradaescolar.dto.EstablecimientoDTO;
import cl.paradaescolar.dto.NivelDTO;
import cl.paradaescolar.model.EstablecimientoModel;
import cl.paradaescolar.model.NivelModel;
import cl.paradaescolar.model.RelEstablecimientoNivelModel;
import cl.paradaescolar.repository.EstablecimientoRepository;
import cl.paradaescolar.repository.NivelRepository;
import cl.paradaescolar.repository.RelEstablecimientoNivelRepository;

/**
 * EstablecimientoService
 */
@Service
public class EstablecimientoService {

    @Autowired
    private EstablecimientoRepository establecimientoRepo;

    @Autowired
    private NivelRepository nivelRepo;

    @Autowired
    private RelEstablecimientoNivelRepository relRepo;

    public EstablecimientoDTO findEstablecimiento(Integer id){

       System.out.println("ID -->" + id); 
       EstablecimientoModel establecimientoModel = establecimientoRepo.findById(id).orElse(new EstablecimientoModel());

       EstablecimientoDTO establecimientoDTO = new EstablecimientoDTO();
       NivelDTO nivelDTO = null;
       NivelModel nivelModel = null;       

       List<NivelDTO> listaNiveles = new ArrayList<>(); 

       establecimientoDTO.setId(establecimientoModel.getId());  
       establecimientoDTO.setNombre(establecimientoModel.getNombre());

       for (RelEstablecimientoNivelModel rel1 : establecimientoModel.getRelEstablecientoNivel()){

        nivelModel = rel1.getNivel();
        nivelDTO = new NivelDTO();
        nivelDTO.setId(nivelModel.getId());
        nivelDTO.setDescripcion(nivelModel.getDescripcion());

        listaNiveles.add(nivelDTO);
       }
       
       establecimientoDTO.setListaNivelDTO(listaNiveles);

       return establecimientoDTO;

        
    }


    public Boolean borrarEstablecimiento(Integer id){

       try{
            establecimientoRepo.deleteById(id);
            
       } catch (Exception e){

            return Boolean.FALSE;
       }
        
       return Boolean.TRUE;


    }   



    public void findAll(){

        
        List<EstablecimientoModel> listEstablecimientoModel = establecimientoRepo.findAll();
        
        System.out.println("TODOS");

       /* listEstablecimientoModel.stream().forEach(p->{
            System.out.println(p.getNombre());

        });*/
     
        
        EstablecimientoModel establecimientoModel2 = listEstablecimientoModel.stream().filter(p-> p.getId().equals(1)).findFirst().get();

        System.out.println(establecimientoModel2.getNombre());

        /* filtrar establecimientos que tengan escuela por nombre */


        List<EstablecimientoModel> respuesta = listEstablecimientoModel.stream()
        .filter(p-> p.getNombre().contains("ESCUELA")).collect(Collectors.toList());


       // respuesta.stream().forEach(System.out::println);


        /*Solo los ids de los colegios que tienen ESCUELA */

        List<Integer> respuesta2 = listEstablecimientoModel.stream()
        .filter(p-> p.getNombre().contains("ESCUELA")).map(EstablecimientoModel::getId).collect(Collectors.toList());

      //  respuesta2.stream().forEach(System.out::println);



      List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8); 
      List<Integer> twoEvenSquares = 
      numbers.stream()    
      .filter(n -> {     
      System.out.println("filtering " + n);   
      return n % 2 == 0;    
      })    
      .map(n -> {     
              return 3;    
      })    
      .limit(2)    
      .collect(Collectors.toList());

     }



     

    public EstablecimientoDTO createEstablecimiento(EstablecimientoDTO establecimientoDTO){

        EstablecimientoModel establecimientoModel = new EstablecimientoModel();
        NivelModel nivelModel = null;
        List<NivelModel> listaNivelModel = new ArrayList<NivelModel>();
        List<RelEstablecimientoNivelModel> listaRelaciones = new ArrayList<RelEstablecimientoNivelModel>();
        RelEstablecimientoNivelModel objetoRelaciones = null;


        establecimientoModel.setNombre(establecimientoDTO.getNombre());
        establecimientoModel.setId(establecimientoDTO.getId());

        for (NivelDTO nivelDto : establecimientoDTO.getListaNivelDTO()){

            nivelModel = new NivelModel();
            nivelModel.setDescripcion(nivelDto.getDescripcion());
            nivelModel.setId(nivelDto.getId());
            
            objetoRelaciones = new RelEstablecimientoNivelModel();

            objetoRelaciones.setCodNivel(nivelModel.getId());
            objetoRelaciones.setCodEstablecimiento(establecimientoModel.getId());
           // objetoRelaciones.setEstablecimiento(establecimientoModel);
            //objetoRelaciones.setNivel(nivelModel);

            listaRelaciones.add(objetoRelaciones);

           // listaNivelModel.add(nivelModel);

        }


        System.out.println("ID DTO ---> "+establecimientoDTO.getId());
        System.out.println("NOMBRE  DTO ---> "+establecimientoDTO.getNombre());

     //   establecimientoModel = establecimientoRepo.save(establecimientoModel);
      //  listaNivelModel = nivelRepo.saveAll(listaNivelModel);
        listaRelaciones = relRepo.saveAll(listaRelaciones);

        System.out.println("ID ESTABLECIMIENTO---> "+establecimientoModel.getId());

        for (NivelModel nivelModelResp : listaNivelModel){
            System.out.println("ID NIVEL---> "+nivelModelResp.getId());
        }


        return establecimientoDTO;
    }

}