package cl.paradaescolar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParadaescolarApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParadaescolarApplication.class, args);
	}

}
