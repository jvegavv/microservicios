package cl.paradaescolar.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
@SuppressWarnings("serial")
public class EstablecimientoDTO implements Serializable {

    private Integer id;
    private String nombre;        
    private List<NivelDTO> listaNivelDTO;

}