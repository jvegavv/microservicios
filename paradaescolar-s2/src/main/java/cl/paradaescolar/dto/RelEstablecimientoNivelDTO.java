package cl.paradaescolar.dto;



import lombok.Data;

@Data
class RelEstablecimientoNivelDTO {
     
    private Long id;  
    private EstablecimientoDTO establecimiento;
    private NivelDTO nivel;
          
     
    // additional properties
    // standard constructors, getters, and setters
}