package cl.paradaescolar.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
@SuppressWarnings("serial")
public class NivelDTO implements Serializable {
      
    private Integer id;
    private String descripcion;    
    private List<RelEstablecimientoNivelDTO> relEstablecientoNivel;



}