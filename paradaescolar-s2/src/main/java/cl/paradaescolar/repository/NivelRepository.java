package cl.paradaescolar.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.paradaescolar.model.NivelModel;

public interface NivelRepository extends JpaRepository<NivelModel, Integer>{


}