package cl.paradaescolar.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.paradaescolar.model.RelEstablecimientoNivelModel;

public interface RelEstablecimientoNivelRepository extends JpaRepository<RelEstablecimientoNivelModel, Integer>{


}