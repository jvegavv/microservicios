package cl.paradaescolar.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.paradaescolar.model.EstablecimientoModel;

public interface EstablecimientoRepository extends JpaRepository<EstablecimientoModel, Integer>{


}