package cl.paradaescolar.model;


import java.io.Serializable;
import java.util.Objects;

import lombok.Data;

@Data
@SuppressWarnings("serial")
public class RelEstablecimientoNivelModelPk implements Serializable {
   
    private Integer codEstablecimiento;    
    private Integer codNivel;

             
    // additional properties
    // standard constructors, getters, and setters

    @Override
  public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      RelEstablecimientoNivelModelPk taskId1 = (RelEstablecimientoNivelModelPk) o;
      if (codEstablecimiento != taskId1.codEstablecimiento) return false;
      return codNivel == taskId1.codNivel;
  }

  @Override
  public int hashCode() {
      return Objects.hash(codEstablecimiento, codNivel);
  }


}