package cl.paradaescolar.model;


import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "pe12_rel_nivel_establecimiento2")
@IdClass(RelEstablecimientoNivelModelPk.class)
@SuppressWarnings("serial")
public class RelEstablecimientoNivelModel implements Serializable {
      
    @Id
    @Column(name = "codestablecimiento", insertable = false, updatable = false)
    private Integer codEstablecimiento;

    @Id
    @Column(name = "codnivel" , insertable = false, updatable = false)
    private Integer codNivel;


    @ManyToOne
    @JoinColumn(name = "codestablecimiento")    
    EstablecimientoModel establecimiento;
 
    @ManyToOne
    @JoinColumn(name = "codnivel")
    NivelModel nivel;
 
    
 
    
     
    // additional properties
    // standard constructors, getters, and setters
}