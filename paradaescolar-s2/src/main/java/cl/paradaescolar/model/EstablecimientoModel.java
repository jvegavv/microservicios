package cl.paradaescolar.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "pe12_establecimiento")
@SuppressWarnings("serial")
public class EstablecimientoModel implements Serializable {


    private static final String ID2 = "id";

    @Id    
    @Column(name = ID2)
    Integer id;

    @Column
    String nombre;
    
    @OneToMany(mappedBy = "establecimiento")
    private List<RelEstablecimientoNivelModel> relEstablecientoNivel;

}