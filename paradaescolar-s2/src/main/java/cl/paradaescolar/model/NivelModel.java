package cl.paradaescolar.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "pe12_nivel")
@SuppressWarnings("serial")
public class NivelModel implements Serializable {

    

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name="id" , unique = true,nullable = false)
    Integer id;

    String descripcion;

    @OneToMany(mappedBy = "nivel")
    private List<RelEstablecimientoNivelModel> relEstablecientoNivel;



}